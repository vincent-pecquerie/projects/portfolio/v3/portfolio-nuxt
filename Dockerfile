FROM node:lts-alpine

EXPOSE 5000
ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=5000

# update and install dependency
RUN apk add --no-cache git libwebp-tools autoconf automake bash g++ libc6-compat libjpeg-turbo-dev \
                       libpng-dev make nasm libtool

# copy the app, note .dockerignore

WORKDIR /home/node
USER node

COPY --chown=node:node package.json package.json
COPY --chown=node:node package-lock.json package-lock.json
RUN npm install

COPY --chown=node:node . .
RUN npm run build

# start the app
CMD [ "npm", "start" ]
