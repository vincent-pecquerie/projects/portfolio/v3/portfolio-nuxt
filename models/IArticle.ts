import { IContentDocument } from '@nuxt/content/types/content'

export interface IArticle extends IContentDocument {
  title: string;
}
