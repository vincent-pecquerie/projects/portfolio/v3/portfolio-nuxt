import DateTimeFormatOptions = Intl.DateTimeFormatOptions;

const dateOptions: DateTimeFormatOptions = {
  year: 'numeric',
  month: 'long',
  day: 'numeric'
}

const dateTimeOptions: DateTimeFormatOptions = {
  year: 'numeric',
  month: 'long',
  day: 'numeric',
  hour: 'numeric',
  minute: 'numeric'
}

export function formatDate (date: string) {
  if (date == null) {
    return 'Aujourd\'hui'
  } else {
    return new Date(date).toLocaleDateString('fr', dateOptions)
  }
}

export function formatDateTime (dateTime: string) {
  if (dateTime == null) {
    return 'A l\'instant'
  } else {
    return new Date(dateTime).toLocaleDateString('fr', dateTimeOptions)
  }
}
