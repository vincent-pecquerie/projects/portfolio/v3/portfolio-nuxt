export class DataService {
  protected readonly itemsPerPage = 5;
  protected readonly contentService: any;
  protected readonly entity: string;

  constructor (contentService: any, entity: string) {
    this.contentService = contentService
    this.entity = entity
  }

  public async count (): Promise<number> {
    const items = await this.contentService(this.entity)
      .only([])
      .fetch()

    return items.length
  }

  public async nbPages (): Promise<Number> {
    const count = await this.count()
    return Math.ceil(count / this.itemsPerPage)
  }

  protected paginateLoad (page: number = 0): any {
    return this.contentService(this.entity)
      .limit(this.itemsPerPage)
      .skip(page * this.itemsPerPage)
  }
}
