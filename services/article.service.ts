import { IContentDocument } from '@nuxt/content/types/content'
import { DataService } from '~/services/data.service'

export class ArticleService extends DataService {
  constructor ($content: any) {
    super($content, 'articles')
  }

  public async load (page: number = 1): Promise<IContentDocument> {
    return await this.paginateLoad(page)
      .sortBy('createdDate', 'desc')
      .fetch()
  }
}
