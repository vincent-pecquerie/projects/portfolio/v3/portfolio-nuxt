export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Vincent PECQUERIE - Portfolio',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Vincent PECQUERIE - Développeur Web, Mobile et aspirant DevOps sur Rouen ou partout ailleurs en télétravail'
      },

      // OG for twitter:
      {
        name: 'twitter:card',
        content: 'summary_large_image'
      },
      {
        name: 'twitter:site',
        content: '@VPecquerie'
      },
      {
        name: 'twitter:image',
        content: 'https://www.vincent-p.fr/social-snap.png'
      },
      {
        hid: 'twitter:description',
        name: 'twitter:description',
        content: 'Vincent PECQUERIE - Développeur Web, Mobile et aspirant DevOps sur Rouen ou partout ailleurs en télétravail'
      },
      {
        hid: 'twitter:title',
        name: 'twitter:title',
        content: 'Vincent PECQUERIE - Portfolio'
      },
      // Og Attributes:
      {
        property: 'og:type',
        content: 'website'
      },
      {
        property: 'og:lang',
        content: 'fr_FR'
      },
      {
        hid: 'og:title',
        property: 'og:title',
        content: 'Vincent PECQUERIE - Portfolio'
      },
      {
        hid: 'og.url',
        property: 'og:url',
        content: 'https://www.vincent-p.fr/'
      },
      {
        hid: 'og:description',
        property: 'og:description',
        content: 'Vincent PECQUERIE - Développeur Web, Mobile et aspirant DevOps sur Rouen ou partout ailleurs en télétravail'
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: 'https://www.vincent-p.fr/social-snap.png'
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      },
      {
        hid: 'canonical',
        rel: 'canonical',
        href: 'https://www.vincent-p.fr/'
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: {
    dirs: [
      '~/components', {
        path: '~/components/layout',
        prefix: 'Layout'
      }
    ]
  },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://www.npmjs.com/package/@nuxtjs/fontawesome
    '@nuxtjs/fontawesome',
    // https://tailwindcss.nuxtjs.org/setup
    '@nuxtjs/tailwindcss',
    // https://image.nuxtjs.org/setup
    '@nuxt/image',
    // https://marquez.co/docs/nuxt-optimized-images/?utm_source=github&utm_medium=readme&utm_campaign=nuxt-optimized-images
    '@aceforth/nuxt-optimized-images'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    // https://github.com/nuxt-community/robots-module
    '@nuxtjs/robots',
    // https://github.com/nuxt-community/sitemap-module
    '@nuxtjs/sitemap'
  ],

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'fr',
      name: 'Vincent PECQUERIE',
      short_name: 'VP',
      description: 'Vincent PECQUERIE - Développeur Web, Mobile et aspirant DevOps sur Rouen ou partout ailleurs en télétravail'
    }
  },

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {
    markdown: {
      prism: {
        theme: 'prism-themes/themes/prism-material-oceanic.css'
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  fontawesome: {
    icons: {
      solid: true,
      brands: true
    }
  },

  /*
  ** Customize the generated output folder
  */
  generate: {
    dir: 'public'
  },

  tailwindcss: {
    jit: true
  },

  optimizedImages: {
    optimizeImages: true
  },

  sitemap: {
    hostname: 'https://www.vincent-p.fr',
  }
}
