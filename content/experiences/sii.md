---
title: Développeur
company: Groupe SII
logo: sii.png
start: 2019-12-05
end: 2020-10-30
location: Mont-Saint-Aignan (76)
---

Dans le cadre du Centre De Services de Rouen, collaboration avec la Banque de France pour un ensemble d'application bancaires:

- Mise en oeuvre d'un guide au bonnes pratiques **Angular** avec une documentation **MkDocs**. 
- Maintient en Condition Opérationnel  d'applications **Delphi** reposant sur une stack technique legacy (SVN, Oracle 11, Delphi XE 7...).
- Application de recommandations sur un socle **JAVA JSF** suite à un audit et au soutient de **SonarQube**.
- Soutient technique pour la migration d'une base de données **Oracle 11g** vers **Oracle 12c** pour un projet à fort historique.
- Conception et Maintenance de **Scripts Shell**.
