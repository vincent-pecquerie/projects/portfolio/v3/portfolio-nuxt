---
title: Développeur Web
company: Société Générale
logo: societe-generale.png
start: 2014-09-01
end: 2017-09-29
location: Paris, La Défense (92)
---

Dans le cadre du contrat de professionnalisation Concepteur développeur Informatique, j’ai effectué mon alternance au sein de l’équipe RESG / GTS / MKT / APS. 
Cette équipe est composée d’architectes d’infrastructure, de chefs de projet et d’expert en sécurité informatique. 
Au sein d’une sous division de développeur, j’ai travaillé sur des outils internes utilisés par les différents collaborateurs répartis sur l’ensemble du globe.

J’ai, dans un premier temps, travaillé sur un moteur de recherches pour retrouver les formulaires de demandes de service conçu. 
Ce projet est une application **PHP/SQL** qui se connecte à un outil tiers pour indexer, rechercher et rediriger l’utilisateur vers le formulaire qu’il souhaite.

J’ai ensuite travaillé sur un outil de **gestion du planning** d’équipe répertoriant les astreintes, les formations, les congés... 
Développé avec le framework web **ZendFramework 2** et utilise l’**ORM Doctrine** pour faire abstraction de la base de données. 
Cet outil est au centre d’un ensemble d’outils tierces qui communiquent au travers de web services REST que nous avons mis en oeuvre. 
J’ai également participé à la préparation de l’infrastructure sur laquelle repose l’application.

Enfin, j'ai travaillé sur un portefeuille de projets client. En effet, les différents chefs de projets mettent en oeuvre des solutions d’infrastructures et ont besoin de répertorier les évolutions de ceux-ci. 
Conçu autour d’une architecture modulaire, nous avons amélioré l’outil existant en axant notre développement autour d’une structure orientées services. 
Développé en **.NET** reposant sur un **serveur IIS** et une **base de données SQL Server**.
