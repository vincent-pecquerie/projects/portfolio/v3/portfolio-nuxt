---
title: Développeur Web, Mobile et DevOps
company: TRSb Digiwin
logo: trsb-digiwin.png
start: 2017-10-02
end: 2019-12-05
location: Bois-Guillaume (76)
---

### Projet 1 : Réalisation d’une application Mobile

Évolution et de correction d'une application web et mobile au travers des technologies Angular / Cordova.
Plateforme iOS et Android avec mode hors ligne via **WebSQL**, **LocalStorage** et service worker.
Évolution sur le back office en **Symfony 3** : des tâches d'import / export de données.
Mise en place avec l’intégration continue avec **Jenkins**, **Docker** mais aussi la compilation automatisée des packages mobiles (APK pour Android et IPA pour iOS) via **Fastlane**.

### Projet 2 : Réalisation d’un socle NodeJS pour application Mobile

Conception d’un Socle backend en **NodeJS** composée en **TypeScript**.
Utilisation de l’ORM **Sequelize**, **Passport.js**.
Déploiement continue du projet avec Jenkins et Docker.

### Projet 3 : Mise en place du CI pour application Mobile.

Mise en place de déploiement continue pour les socles mobiles / web (React Native, Ionic, Xamarin).
Ajout d’un agent Jenkins sur un mac pour la compilation des packages iOS avec **Fastlane**.

### Projet 4 : Développement d’une librairie de composant.

Réalisation d’une librairie de **composants Angular** pour les projets Angular.
Utilisation de l’outil **Compodoc** pour les documenter.

### Projet 5 : Développement d’une application Mobile.

Réalisation d’une application via la librairie créée.
Création de Web Services sur un socle basée sur le **CMS OpenCart** existant.
Ajout du moteur de migration SQL **Phinx**.
Déploiement d'une instance **Gitlab**.
Mise en place de l’**intégration continue** et du socle Docker.

### Projet 6 : Mise en place d’un serveur d’intégration.

Mise en place un serveur d’intégration pour les projets web et mobile du client : **Docker**, reverse proxy **Traefik**, Gitlab et Gitlab CI.

### Projet 7 : DevOps et Scrum Master Progressive Web App.

Utilisation du framework de gestion de projet **Scrum**.
Projet développé en **TDD** autour du framework **Ionic**.
Mise en place de l’intégration et le déploiement continu du projet avec **Gitlab CI**.

### Projet 8 : Extranet.

Migrations et évolutions d’un site web codé en **WebForms** vers une structure en **.NET Core**.
Mise en place du protocole **OAuth2**.
