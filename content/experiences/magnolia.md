---
title: Développeur Web FullStack (JAVA / Angular)
company: Magnolia.fr
logo: magnolia.png
start: 2020-11-02
location: Mont-Saint-Aignan (76)
---

Au sein de la DSI du groupe Magnolia.fr, mise en oeuvre de mes compétences au profit du tarificateur et de l'espace client du groupe.
D'abord développeur, puis Lead Développeur et enfin Team Leader.

Environnement technique:

* Java 11
* SpringBoot, Spring Cloud, Spring Data (avec framework JHipster)
* Apache Kafka
* MongoDB
* AWS (EC2, S3, DynamoDB) et Terraform
* Kubernetes (cluster auto hébergé) + Helm

Tâches de développements:

* Développement, Maintenance et création de MicroServices en JAVA.
* Intégration de produit d'assurance emprunteur dans le tarificateur.
* Développement d'API pour enrichir l'espace de souscription.
* Evolutions au sein du Front-End de l'espace client (Intégration d'outil externes)

Tâches de Lead:

* Code Review + partage des bonnes pratiques de développement
* Mise en place des Barrières qualités SonarQube
* Mise en place de Sentry pour monitorer et aggréger les métriques et logs.
* Mise en place de documentation et processus (Livraison, Rollback, Tests, ...).

Tâches de DevOps:

* Mise en place et amélioration de l'intégration continue et du tooling (Gitlab CI, SonarQube, Docker).
* Conception et maintenance de l'architecture MicroServices
* Gestion du Support N2 Infra en cas d'absences.

Tâche de Team Leader:

* Entretiens annuels d'évaluations
* Feedback constructifs sur les axes d'améliorations
* Définition et revue des objectifs annuels ou semestriels
* Mise en place de créneau privilégié (OneToOne)
* Mise en place d'un plan de transfert de compétence pour rendre l'équipe d'avantage polyvalente.