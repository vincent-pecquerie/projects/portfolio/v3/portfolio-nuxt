---
title: "Licence Professionnelle: Concepteur Développeur Informatique"
school: NextAdvance 
logo: nextadvance.png
start: 2014-09-01
end: 2015-08-31
location: Paris 8ème (75)
---

Formation de Concepteur Développeur Informatique, on y découvre l'écosystème autour de la programmation 
dont la gestion de projet, l'utilisation de web services ou la réalisation de diagrammes UML.


### Les modules de formations :

* Architecture des ordinateurs
* Programmation Procédurale
* Programmation Orientée Objet
* Fondamentaux du réseau pour les développeurs
* Bases d'Unix
* HTML/CSS
* Intégration Web
* Javascript, Ajax et jQuery
* Programmation PHP
* Java Standard
* Java pour le Web
* SQL et MySQL
* Merise (couche données)
* UML
* Gestion de projet Web et Méthodes Agile (Scrum)
* Veille Technologique
* XML
* Design Pattern
