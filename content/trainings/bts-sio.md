---
title: BTS Services Informatiques aux Organisation
school: Lycée Gustave Flaubert
logo: flaubert.png
start: 2012-09-01
end: 2014-08-31
location: Rouen (76)
---

Le BTS SIO option Spécialité Logicielles et Applications Métiers forme au développement d'application.

L’étudiant doit être capable de suivre l'ensemble du cycle de vie des applications :

* Analyser les besoins des utilisateurs finaux
* Rechercher et choisir une solution adaptée
* Concevoir et développer des applications (génie logiciel)
* Assurer la maintenance des programmes qui garantissent le bon fonctionnement de ces applications
* …

Le développeur d'applications, encore appelé programmeur ou analyste-programmeur, maîtrise les méthodes et les outils de traitement de l'information et de conduite de projets informatiques. Il doit rester à l'écoute de l'évolution du marché de l'informatique.
