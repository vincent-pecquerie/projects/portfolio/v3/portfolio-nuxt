---
title: "Master Professionnel: Expert en Système d'Information"
school: NextAdvance Vincennes (94)
logo: nextadvance.png
start: 2015-09-22
end: 2017-09-22
location: Vincennes (94)
---

Formation d'Expert en Système d'Information, on y approfondit l'écosystème autour de la programmation dont la gestion de projet, l'utilisation de web services ou la réalisation de diagrammes UML mais aussi de l'architecture d'infrastructure.

### Les modules de formations :

* Modélisation (Merise – UML)
* PHP/MySQL
* JAVA
* Langage C
* C sous Linux
* C#.net
* Python
* SGBDR (SqlServer, Oracle, MySQL)
* Développement mobile (Android, Windows Phone, iOS)
* Réseaux d'entreprise (VLAN, Routage, Firewall, LDAP,...)
* Administration Windows et Linux
* Sécurité des SI
* Design Pattern
