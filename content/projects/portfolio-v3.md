---
ordinal: 2
name: Portfolio V3
repositoryUrl: https://gitlab.com/VPecquerie/portfolio-nuxt
pictureUrl: /projects/portfolio-v3.png
---

Troisième version de mon portfolio. Front-end en NuxtJS (Vue.js / TS) généré en SSG avec pour source de données des fichiers Markdown.

