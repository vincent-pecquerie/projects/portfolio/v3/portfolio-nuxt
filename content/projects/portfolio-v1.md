---
ordinal: 4
name: Portfolio V1
repositoryUrl: 
pictureUrl: /projects/portfolio-v1.png
demoUrl: https://v1.vincent-p.fr
---

Première version de mon portfolio. Front-end en Angular 2 avec une API en .NET Core et une base de données SQL Serveur

