---
ordinal: 1
name: Dotfiles
repositoryUrl: https://gitlab.com/vincent-pecquerie/projects/environment/dotfiles
pictureUrl: /projects/dotfiles.png
---

Ce projet regroupe l'ensemble des configurations de mon environnement de développement. 
Cela repose sur des fichiers de configuration pour chaque outils que j'utilise (ZSH, VIM, SSH, ...)


