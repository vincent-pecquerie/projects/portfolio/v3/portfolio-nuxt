---
ordinal: 3
name: Portfolio V2
repositoryUrl: https://gitlab.com/www.vincent-p.fr/portfolio
pictureUrl: /projects/portfolio-v2.png
demoUrl: https://v2.vincent-p.fr
---

Seconde version de mon portfolio. Front-end en Angular 10 avec une API en nodejs via le framework NestJS et une base de données MariaDb

